<?php

/*
* Job Listings
*/
function custom_post_jobs() {
  $labels = array(
    'name'               => _x( 'Job Listings', 'post type general name' ),
    // 'name'               => _x( 'Portfolio Projects', 'post type general name' ),
    'singular_name'      => _x( 'Job Listing', 'post type singular name' ),
    'add_new'            => _x( 'Add new listing', 'jobs' ),
    'add_new_item'       => __( 'Add new listing' ),
    'edit_item'          => __( 'Edit listing' ),
    'new_item'           => __( 'New listing' ),
    'all_items'          => __( 'Job listings' ),
    'view_item'          => __( 'View listing' ),
    'search_items'       => __( 'Search job listings' ),
    'not_found'          => __( 'No job listings found' ),
    'not_found_in_trash' => __( 'No job listings found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Job Listings'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds job listing and job listing data',
    'public'        => true,
    'menu_position' => 50,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon' => 'dashicons-clipboard',
    'has_archive'   => false,
  );
  register_post_type( 'careers', $args );
}
add_action( 'init', 'custom_post_jobs' );

function remove_custom_post_jobs_comment() {
    remove_post_type_support( 'careers', 'comments' );
}
add_action( 'init', 'remove_custom_post_jobs_comment' );

/*
* Job listing saving
*/
function updated_jobs( $jobs ) {
  global $post, $post_ID;
  $jobs['jobs'] = array(
    0 => '',
    1 => sprintf( __('Job listing updated. <a href="%s">View listing</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Job listing updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Job listing restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Job listing published. <a href="%s">View job listing</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Job listing saved.'),
    8 => sprintf( __('Job listing submitted. <a target="_blank" href="%s">Preview job listing</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Job listing scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview job listing</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Job listing draft updated. <a target="_blank" href="%s">Preview job listing</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $jobs;
}
add_filter( 'post_updated_messages', 'updated_jobs' );

/*
* Press Release
*/
function custom_post_pressreleases() {
  $labels = array(
    'name'               => _x( 'Press Releases', 'post type general name' ),
    // 'name'               => _x( 'Portfolio Projects', 'post type general name' ),
    'singular_name'      => _x( 'Press Release', 'post type singular name' ),
    'add_new'            => _x( 'Add new press release', 'announcements' ),
    'add_new_item'       => __( 'Add new press release' ),
    'edit_item'          => __( 'Edit press release' ),
    'new_item'           => __( 'New press release' ),
    'all_items'          => __( 'Press Releases' ),
    'view_item'          => __( 'View press release' ),
    'search_items'       => __( 'Search press releases' ),
    'not_found'          => __( 'No press releases found' ),
    'not_found_in_trash' => __( 'No press releases found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Press Releases'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds press releases and press release data',
    'public'        => true,
    'publicly_queryable'  => true,
    'menu_position' => 50,
    'supports'      => array('title', 'editor' ),
    'menu_icon' => 'dashicons-media-text',
    'has_archive'   => false,
		'rewrite' => array('slug' => 'press-release'),
  );
  register_post_type( 'pressreleases', $args );
}
add_action( 'init', 'custom_post_pressreleases' );

/*
* Press Release saving
*/
function updated_pressreleases( $pressrelease ) {
  global $post, $post_ID;
  $pressrelease['pressreleases'] = array(
    0 => '',
    1 => sprintf( __('Press Release updated. <a href="%s">View listing</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Press Release updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Job listing restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Press Release published. <a href="%s">View press release</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Press Release saved.'),
    8 => sprintf( __('Press Release submitted. <a target="_blank" href="%s">Preview product</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Press Release scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview product</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Press Release draft updated. <a target="_blank" href="%s">Preview product</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $pressrelease;
}
add_filter( 'post_updated_pressreleases', 'updated_pressrelease' );

// /*
// * Job Listing Applications
// */
// function custom_post_applications() {
//   $labels = array(
//     'name'               => _x( 'Applications', 'post type general name' ),
//     // 'name'               => _x( 'Portfolio Projects', 'post type general name' ),
//     'singular_name'      => _x( 'Application', 'post type singular name' ),
//     'add_new'            => _x( 'Add new application', 'application' ),
//     'add_new_item'       => __( 'Add new application' ),
//     'edit_item'          => __( 'Edit application' ),
//     'new_item'           => __( 'New application' ),
//     'all_items'          => __( 'Submitted applications' ),
//     'view_item'          => __( 'View application' ),
//     'search_items'       => __( 'Search job application' ),
//     'not_found'          => __( 'No applications found' ),
//     'not_found_in_trash' => __( 'No applications found in the Trash' ),
//     'parent_item_colon'  => '',
//     'menu_name'          => 'Submitted Applications'
//   );
//   $args = array(
//     'labels'        => $labels,
//     'description'   => 'Holds application data',
//     'public'        => true,
//     'menu_position' => 50,
//     'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
//     'menu_icon' => 'dashicons-id-alt',
//     'has_archive'   => false,
//     'show_in_menu' => 'edit.php?post_type=careers'
//   );
//   register_post_type( 'applications', $args );
// }
// add_action( 'init', 'custom_post_applications' );
//
// function remove_custom_post_applications_comment() {
//     remove_post_type_support( 'applications', 'comments' );
// }
// add_action( 'init', 'remove_custom_post_applications_comment' );
//
// /*
// * Job Application saving
// */
// function updated_applications( $jobs ) {
//   global $post, $post_ID;
//   $jobs['jobs'] = array(
//     0 => '',
//     1 => sprintf( __('Job listing updated. <a href="%s">View listing</a>'), esc_url( get_permalink($post_ID) ) ),
//     2 => __('Custom field updated.'),
//     3 => __('Custom field deleted.'),
//     4 => __('Job listing updated.'),
//     5 => isset($_GET['revision']) ? sprintf( __('Job listing restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
//     6 => sprintf( __('Job listing published. <a href="%s">View job listing</a>'), esc_url( get_permalink($post_ID) ) ),
//     7 => __('Job listing saved.'),
//     8 => sprintf( __('Job listing submitted. <a target="_blank" href="%s">Preview job listing</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
//     9 => sprintf( __('Job listing scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview job listing</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
//     10 => sprintf( __('Job listing draft updated. <a target="_blank" href="%s">Preview job listing</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
//   );
//   return $jobs;
// }
// add_filter( 'post_updated_messages', 'updated_applications' );

/*
* Job Listing Applications
*/
function custom_post_contactforms() {
  $labels = array(
    'name'               => _x( 'Contact Form', 'post type general name' ),
    // 'name'               => _x( 'Portfolio Projects', 'post type general name' ),
    'singular_name'      => _x( 'Contact Form', 'post type singular name' ),
    'add_new'            => _x( 'Add contact form', 'application' ),
    'add_new_item'       => __( 'Add contact form' ),
    'edit_item'          => __( 'Edit submission' ),
    'new_item'           => __( 'New submission' ),
    'all_items'          => __( 'Contact form submissions' ),
    'view_item'          => __( 'View contact form submissions' ),
    'search_items'       => __( 'Search contact form submissions' ),
    'not_found'          => __( 'No contact form submissions found' ),
    'not_found_in_trash' => __( 'No contact form submissions found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Contact Form'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds contact form submission data',
    "public" => false,
    "publicly_queryable" => false,
    "show_ui" => true,
    'menu_position' => 50,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'menu_icon' => 'dashicons-format-status',
    'has_archive'   => false,
    // 'show_in_menu' => 'edit.php?post_type=careers'
  );
  register_post_type( 'contactforms', $args );
}
add_action( 'init', 'custom_post_contactforms' );

function remove_custom_post_contactforms_comment() {
    remove_post_type_support( 'contactforms', 'comments' );
}
add_action( 'init', 'remove_custom_post_contactforms_comment' );

/*
* Job Application saving
*/
function updated_contactforms( $jobs ) {
  global $post, $post_ID;
  $contactforms['contactforms'] = array(
    0 => '',
    1 => sprintf( __('Contact form updated. <a href="%s">View contact form</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Contact form updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Contact form restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Contact form published. <a href="%s">View contact form</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Contact form saved.'),
    8 => sprintf( __('Contact form submitted. <a target="_blank" href="%s">Preview contact form</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Contact form scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview contact form</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Contact form draft updated. <a target="_blank" href="%s">Preview job listing</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $contactforms;
}
add_filter( 'post_updated_messages', 'updated_contactforms' );
