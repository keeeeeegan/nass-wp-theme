<?php
/**
 * Template Name: Job Openings Sidebar
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
<?php
  if (has_post_thumbnail()) {
    $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
  } else {
    $featured_image_url = null;
  }
?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php if ($featured_image_url): ?>
      <?php echo '<div class="featured-image" style="background-image:url(' . $featured_image_url . ');"></div>'; ?>
    <?php endif; ?>

    <div class="article-wrapper">
  		<article class="post with-sidebar" id="post-<?php the_ID(); ?>">


				<div class="heading-wrapper">
					<h2><?php the_title(); ?></h2>
				</div>

  			<?php /*posted_on();*/ ?>

  			<div class="entry">

  				<?php the_content(); ?>

  				<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

  			</div>

				<?php if (get_theme_mod('show_hiring')): ?>
					<aside class="sidebar">
	          <h1>Interested in Joining Our Team?</h1>
	          <p class="read-more">
							<a href="<?php echo get_theme_mod('hiring_link'); ?>">Check out open positions &raquo;</a>
						</p>
	        </aside>
				<?php endif; ?>
  		</article>
    </div>

		<?php endwhile; endif; ?>

<?php get_footer(); ?>
