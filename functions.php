<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */

  	require_once('custom-post-types.php');

	// Options Framework (https://github.com/devinsays/options-framework-plugin)
	if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/_/inc/' );
		require_once dirname( __FILE__ ) . '/_/inc/options-framework.php';
	}

	// Theme Setup (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_setup() {
		load_theme_textdomain( 'html5reset', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'structured-post-formats', array( 'link', 'video' ) );
		add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status' ) );
		register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
		register_nav_menu( 'secondary', __( 'Footer Navigation Menu', 'html5reset' ) );
		add_theme_support( 'post-thumbnails' );
	}
	add_action( 'after_setup_theme', 'html5reset_setup' );

	// Scripts & Styles (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_scripts_styles() {
		global $wp_styles;

		// Load Comments
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

		// Load Stylesheets
//		wp_enqueue_style( 'html5reset-reset', get_template_directory_uri() . '/reset.css' );
//		wp_enqueue_style( 'html5reset-style', get_stylesheet_uri() );

		// Load IE Stylesheet.
//		wp_enqueue_style( 'html5reset-ie', get_template_directory_uri() . '/css/ie.css', array( 'html5reset-style' ), '20130213' );
//		$wp_styles->add_data( 'html5reset-ie', 'conditional', 'lt IE 9' );

		// Modernizr
		// This is an un-minified, complete version of Modernizr. Before you move to production, you should generate a custom build that only has the detects you need.
		// wp_enqueue_script( 'html5reset-modernizr', get_template_directory_uri() . '/_/js/modernizr-2.6.2.dev.js' );

	}
	add_action( 'wp_enqueue_scripts', 'html5reset_scripts_styles' );

	// WP Title (based on twentythirteen: http://make.wordpress.org/core/tag/twentythirteen/)
	function html5reset_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() )
			return $title;

//		 Add the site name.
		$title .= get_bloginfo( 'name' );

//		 Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";

//		 Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'html5reset' ), max( $paged, $page ) );

		return $title;
	}
	add_filter( 'wp_title', 'html5reset_wp_title', 10, 2 );




//OLD STUFF BELOW


	// Load jQuery
	if ( !function_exists( 'core_mods' ) ) {
		function core_mods() {
			if ( !is_admin() ) {
				wp_deregister_script( 'jquery' );
				wp_register_script( 'jquery', ( "//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ), false);
				wp_enqueue_script( 'jquery' );
			}
		}
		add_action( 'wp_enqueue_scripts', 'core_mods' );
	}

	// Clean up the <head>, if you so desire.
	//	function removeHeadLinks() {
	//    	remove_action('wp_head', 'rsd_link');
	//    	remove_action('wp_head', 'wlwmanifest_link');
	//    }
	//    add_action('init', 'removeHeadLinks');

	// Custom Menu
	register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );

	// Widgets
	if ( function_exists('register_sidebar' )) {
		function html5reset_widgets_init() {
			register_sidebar( array(
				'name'          => __( 'Sidebar Widgets', 'html5reset' ),
				'id'            => 'sidebar-primary',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
		}
		add_action( 'widgets_init', 'html5reset_widgets_init' );
	}

	// Navigation - update coming from twentythirteen
	function post_navigation() {
		echo '<div class="navigation">';
		echo '	<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
		echo '	<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
		echo '</div>';
	}

	// Posted On
	function posted_on() {
		printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_author() )
		);
	}
	/*
	* Init custom child theme stuffs
	*/
	function theme_enqueue_styles() {
      wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:400,700,400italic|Roboto:300,400,500,400italic,500italic,700,900' );
      wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
      //wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Montserrat|Roboto+Condensed:400,700,400italic|Roboto:300,400,500,400italic,500italic,700,900' );
      wp_enqueue_style( 'nass-theme', get_template_directory_uri() . '/assets/css/style.css' );
      wp_enqueue_script( 'nass-script', get_template_directory_uri() . '/assets/js/nass-min.js' );
      wp_enqueue_script( 'nass-app', get_template_directory_uri() . '/assets/js/app-min.js' );
	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

  function theme_custom_init() {
    remove_post_type_support( 'page', 'comments' );
  }
  add_action( 'init', 'theme_custom_init' );

	/*
	* Remove menu items from admin
	*/
	function admin_remove_menus () {
	  global $menu;
	  //$restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));

	  // for client, disable these
	  //$restricted = array(__('Posts'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
	  $restricted = array(__('Posts'), __('Comments'));

	  end ($menu);
	  while (prev($menu)){
	    $value = explode(' ',$menu[key($menu)][0]);
	    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
	  }
	}
	add_action('admin_menu', 'admin_remove_menus');

	/*
	* List child pages with content
	*/
	function sc_list_child_pages_content( $atts ) {
	    //return execute Butts;
	    //Program;
	  global $post;
	  $html = '';

    // attributes
    extract(shortcode_atts(array(
        'heading' => 'h3',
     ), $atts));

	  $args = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $post->ID,
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
	   );

	  $parent = new WP_Query( $args );

	  if ( $parent->have_posts() ) {
	    while ( $parent->have_posts() ) {
	      $parent->the_post();
	      $html .= '<' . $heading . '><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></' . $heading . '>';
	      $html .= get_the_content();
	    }
	  }
	  wp_reset_query();

	  return $html;
	}
	add_shortcode('list-child-page-content', 'sc_list_child_pages_content');

	/*
	* List child page links
	*/
	function sc_list_child_pages() {
	  global $post;
	  $html = '';

	  $args = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $post->ID,
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
	   );

	  $parent = new WP_Query( $args );

	  if ( $parent->have_posts() ) {
	    $html .= "<ul>";

	    while ( $parent->have_posts() ) {
	      $parent->the_post();
	      $html .= '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
	    }

	    $html .= "</ul>";
	  }
	  wp_reset_query();

	  return $html;
	}
	add_shortcode('list-child-pages', 'sc_list_child_pages');

	/*
	* List child page links
	*/
	function sc_list_jobs( $atts ) {
	  global $post;
	  $html = '';

    $a = shortcode_atts( array(
            'max' => -1,
            'heading' => null,
            'show_all_link' => true,
        ), $atts );

	  $args = array(
	    'post_type'      => 'careers',
	    'posts_per_page' => $a['max'],
	    'show_all_link' => $a['show_all_link'],
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
	   );

	  $posts = new WP_Query( $args );

	  if ( $posts->have_posts() ) {

      if ( $a['heading'] ) {
        $html .= "<h3>" . $a['heading'] . "</h3>";
      }

	    $html .= "<ul>";

	    while ( $posts->have_posts() ) {
	      $posts->the_post();
	      $html .= '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
	    }

	    $html .= "</ul>";
      if ($a['show_all_link'] !== "no") {
        $html .= '<a href="' . get_site_url() . '/careers" class="show-all">See all openings &raquo;</a>';
      }
	  }
    else {
      $html .= '<p class="flash notice">There are no job openings posted at this time. Please check back soon!</p>';
    }
	  wp_reset_query();

	  return $html;
	}
	add_shortcode('list-jobs', 'sc_list_jobs');


  /*
	* Contact Form
	*/

  function nass_submit_contact_form() {

    // if the submit button is clicked, send the email
    if ( isset( $_POST['cf_submitted'] ) ) {

        // sanitize form values
        $name    = sanitize_text_field( $_POST["cf_name"] );
        $email   = sanitize_email( $_POST["cf_email"] );
        $phone   = sanitize_text_field( $_POST["cf_phone"] );
        $original_message = esc_textarea( $_POST["cf_message"] );
        $subject = "New Contact Form Submission";
        $message = "You have been sent a message via the contact form at nassusa.com/contact.\r\n\r\n";
        $message .= "Name: " . $name . "\r\n";
        $message .= "Email: " . $email . "\r\n";
        $message .= "Phone: " . $phone . "\r\n\r\n";
        $message .= "Message: \r\n";
        $message .= $original_message;

        // get the blog administrator's email address
        $to = get_theme_mod( 'contact_form_email' );

        //$headers = "From: $name <$email>" . "\r\n";

        $post_id = nass_create_submission($name, substr($original_message, 0, 35) . '...', $email, $phone, $original_message);

        $message .= "\r\n\r\n";
        $message .= "You can also view this message in the admin section of the website ";
        $message .= 'by visiting the following url: ' . get_admin_url() . 'post.php?post=' . $post_id . '&action=edit';

        // If email has been process for sending, display a success message
        if ( wp_mail( $to, $subject, $message, $headers ) ) {
          echo '<div class="flash thank-you" id="thanks">';
          echo '<p>Thank you for contacting us! Expect a response from one of our representatives shortly. If you are in need of immediate assistance, please call us at ' . get_theme_mod( 'company_phone' ) . '.</p>';
          echo '</div>';
        } else {
          echo '<div class="flash error">';
          echo '<p>An unexpected error occurred.</p>';
          echo '</div>';
        }
        unset( $_POST['cf_submitted'] );
    }
  }

  function nass_contact_form( $atts ) {
    ob_start();
    nass_submit_contact_form();
    ?>
    <form action="/contact#thanks" method="POST" class="contact-form" target="_self">
      <label>Name:</label>
      <input type="text" name="cf_name">

      <label>Email:</label>
      <input type="email" name="cf_email">

      <label>Phone:</label>
      <input type="text" name="cf_phone">

      <label>Message:</label>
      <textarea name="cf_message"></textarea>
      <input type="hidden" name="cf_submitted" value="submitted" />
      <input type="submit" class="button submit" value="Send">
    </form>
    <?php
    $output = ob_get_contents();
    ob_end_clean();

    return $output;
	}
	add_shortcode('contact-form', 'nass_contact_form');

  // @TODO: error checking
  function nass_config_output( $atts ) {

    // map mod to mod name to allow shortcode
    $allowed_mods = array(
      'phone' => 'company_phone',
      'fax' => 'company_fax',
      'email' => 'company_email'
    );

    // loop through mutlidimentional array
    foreach ( $atts as $att ) {
      foreach ($allowed_mods as $mod => $val) {
        if ($mod == $att) {
          $theme_mod = get_theme_mod($allowed_mods[$att]);

          // @TODO pull this out and make it more generic
          if ($val == 'company_email') {
            return '<a href="mailto:' . $theme_mod . '">' . $theme_mod . '</a>';
          }
          return $theme_mod;
        }
      }
    }
	}
	add_shortcode('contact', 'nass_config_output');

  function nass_create_submission($name, $_subject, $email, $phone, $_message) {

    $subject = $name . ' - ' . $_subject;
    $message = "Name: " . $name . "\r\n";
    $message .= "Email: " . $email . "\r\n";
    $message .= "Phone: " . $phone . "\r\n\r\n";
    $message .= "Message: \r\n";
    $message .= $_message;

    // Create post object
    $my_post = array(
      'post_type'      => 'contactforms',
      'post_title'    => $subject,
      'post_content'  => $message,
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_category' => array(8,39)
    );

    // Insert the post into the database
    return wp_insert_post( $my_post );

    // $post = array(
    //   'ID'             => [ <post id> ] // Are you updating an existing post?
    //   'post_content'   => [ <string> ] // The full text of the post.
    //   'post_name'      => [ <string> ] // The name (slug) for your post
    //   'post_title'     => [ <string> ] // The title of your post.
    //   'post_status'    => [ 'draft' | 'publish' | 'pending'| 'future' | 'private' | custom registered status ] // Default 'draft'.
    //   'post_type'      => [ 'post' | 'page' | 'link' | 'nav_menu_item' | custom post type ] // Default 'post'.
    //   'post_author'    => [ <user ID> ] // The user ID number of the author. Default is the current user ID.
    //   'ping_status'    => [ 'closed' | 'open' ] // Pingbacks or trackbacks allowed. Default is the option 'default_ping_status'.
    //   'post_parent'    => [ <post ID> ] // Sets the parent of the new post, if any. Default 0.
    //   'menu_order'     => [ <order> ] // If new post is a page, sets the order in which it should appear in supported menus. Default 0.
    //   'to_ping'        => // Space or carriage return-separated list of URLs to ping. Default empty string.
    //   'pinged'         => // Space or carriage return-separated list of URLs that have been pinged. Default empty string.
    //   'post_password'  => [ <string> ] // Password for post, if any. Default empty string.
    //   'guid'           => // Skip this and let Wordpress handle it, usually.
    //   'post_content_filtered' => // Skip this and let Wordpress handle it, usually.
    //   'post_excerpt'   => [ <string> ] // For all your post excerpt needs.
    //   'post_date'      => [ Y-m-d H:i:s ] // The time post was made.
    //   'post_date_gmt'  => [ Y-m-d H:i:s ] // The time post was made, in GMT.
    //   'comment_status' => [ 'closed' | 'open' ] // Default is the option 'default_comment_status', or 'closed'.
    //   'post_category'  => [ array(<category id>, ...) ] // Default empty.
    //   'tags_input'     => [ '<tag>, <tag>, ...' | array ] // Default empty.
    //   'tax_input'      => [ array( <taxonomy> => <array | string>, <taxonomy_other> => <array | string> ) ] // For custom taxonomies. Default empty.
    //   'page_template'  => [ <string> ] // Requires name of template file, eg template.php. Default empty.
    // );
  }

  function nass_customizer( $wp_customize ) {
    // $wp_customize->add_panel();
    // $wp_customize->get_panel();
    // $wp_customize->remove_panel();

    // $wp_customize->add_section();
    // $wp_customize->get_section();
    // $wp_customize->remove_section();

    // $wp_customize->add_setting();
    // $wp_customize->get_setting();
    // $wp_customize->remove_setting();

    // $wp_customize->add_control();
    // $wp_customize->get_control();
    // $wp_customize->remove_control();

    $wp_customize->add_section( 'contact', array(
      'title' => _( 'Contact Information' ),
      'description' => __( '' ),
      'capability' => 'edit_theme_options'
    ) );

    $wp_customize->add_setting( 'company_phone', array(
      'type' => 'theme_mod',
      'default' => '407.555.5555',
    ) );
    $wp_customize->add_control( 'company_phone', array(
      'label' => __( 'Phone Number' ),
      'type' => 'text',
      'section' => 'contact',
    ) );

    $wp_customize->add_setting( 'company_fax', array(
      'type' => 'theme_mod',
      'default' => '407.551.5555',
    ) );
    $wp_customize->add_control( 'company_fax', array(
      'label' => __( 'Fax Number' ),
      'type' => 'text',
      'section' => 'contact',
    ) );

    $wp_customize->add_setting( 'company_email', array(
      'type' => 'theme_mod',
      'default' => 'info@email.com',
    ) );
    $wp_customize->add_control( 'company_email', array(
      'label' => __( 'Company Email' ),
      'description' => __( 'Email for general contact inquiries.' ),
      'type' => 'text',
      'section' => 'contact',
    ) );

    $wp_customize->add_setting( 'contact_form_email', array(
      'type' => 'theme_mod',
      //'default' => 'barbara@nassusa.com',
      'default' => 'email@website.com',
    ) );
    $wp_customize->add_control( 'contact_form_email', array(
      'label' => __( 'Contact Form Email' ),
      'description' => __( 'Contact form submissions will be sent to this address.' ),
      'type' => 'text',
      'section' => 'contact',
    ) );

    $wp_customize->add_section( 'options_settings', array(
      'title' => _( 'Options' ),
      'description' => __( '' ),
      'capability' => 'edit_theme_options'
    ) );

    $wp_customize->add_setting( 'company_description', array(
      'type' => 'theme_mod',
      'default' => 'North American Substation Services (NASS) is a nationally-recognized expert specializing in transformer and substation apparatus installation, services, and repair. Our management and highly skilled field-based force have received specialized training and are among the most experienced in the industry. NASS is dedicated to providing quality service and solutions that optimize the reliability of transformers and other substation equipment. Our customers know us as a trusted resource in the substation field with established long-term relationships grounded in integrity, professionalism, and value.',
    ) );
    $wp_customize->add_control( 'company_description', array(
      'label' => __( 'Company Description' ),
      'type' => 'textarea',
      'description' => __( 'Description shown on homepage.' ),
      'section' => 'options_settings',
    ) );

    $wp_customize->add_setting( 'company_short_description', array(
      'type' => 'theme_mod',
      'default' => 'NASS is a market-leading expert independent provider of installation, repair, and maintenance services to mission critical high voltage substation equipment throughout the United States.',
    ) );
    $wp_customize->add_control( 'company_short_description', array(
      'label' => __( 'Company Short Description' ),
      'type' => 'textarea',
      'description' => __( 'Description shown in footer.' ),
      'section' => 'options_settings',
    ) );

    $wp_customize->add_setting( 'company_seo_description', array(
      'type' => 'theme_mod',
      'default' => 'NASS is a market-leading expert independent provider of installation, repair, and maintenance services to mission critical high voltage substation equipment throughout the United States.',
    ) );
    $wp_customize->add_control( 'company_seo_description', array(
      'label' => __( 'Company SEO Description' ),
      'type' => 'textarea',
      'description' => __( 'Description used in meta description for SEO purposes.' ),
      'section' => 'options_settings',
    ) );

		$wp_customize->add_setting( 'show_hiring', array(
      'type' => 'theme_mod',
      'default' => false,
    ) );
    $wp_customize->add_control( 'show_hiring', array(
      'label' => __( 'Show Hiring Box' ),
      'type' => 'checkbox',
      'description' => __( 'Show the "now hiring" box on the homepage (unchecked to hide).' ),
      'section' => 'options_settings',
    ) );

		$wp_customize->add_setting( 'hiring_label', array(
      'type' => 'theme_mod',
      'default' => "We're Hiring!",
    ) );
    $wp_customize->add_control( 'hiring_label', array(
      'label' => __( 'Hiring Heading' ),
      'type' => 'text',
      'description' => __( 'Text used for heading on the "now hiring" box on the homepage (if "Show Hiring Box" is checked).' ),
      'section' => 'options_settings',
    ) );

		$wp_customize->add_setting( 'hiring_link', array(
      'type' => 'theme_mod',
      'default' => "",
    ) );
    $wp_customize->add_control( 'hiring_link', array(
      'label' => __( 'Hiring Link' ),
      'type' => 'text',
      'description' => __( 'Link set for "now hiring" box on the homepage (if "Show Hiring Box" is checked).' ),
      'section' => 'options_settings',
    ) );

    $wp_customize->add_setting( 'hiring_copy', array(
      'type' => 'theme_mod',
      'default' => "",
    ) );
    $wp_customize->add_control( 'hiring_copy', array(
      'label' => __( 'Hiring Message' ),
      'type' => 'textarea',
      'description' => __( 'Text to be displayed underneath the heading on the "now hiring" box on the homepage (if "Show Hiring Box" is checked). Empty by default.' ),
      'section' => 'options_settings',
    ) );

		$wp_customize->add_setting( 'show_announcement', array(
      'type' => 'theme_mod',
      'default' => true,
    ) );
    $wp_customize->add_control( 'show_announcement', array(
      'label' => __( 'Show Announcement Box' ),
      'type' => 'checkbox',
      'description' => __( 'Show the announcement box on the homepage (unchecked to hide).' ),
      'section' => 'options_settings',
    ) );

		$wp_customize->add_setting( 'announcement_label', array(
      'type' => 'theme_mod',
      'default' => "Announcements",
    ) );
    $wp_customize->add_control( 'announcement_label', array(
      'label' => __( 'Announcement Heading' ),
      'type' => 'text',
      'description' => __( 'Text used for heading on the announcement box on the homepage (if "Show Announcement Box" is checked).' ),
      'section' => 'options_settings',
    ) );

		$wp_customize->add_setting( 'announcement_link', array(
      'type' => 'theme_mod',
      'default' => "/",
    ) );
    $wp_customize->add_control( 'announcement_link', array(
      'label' => __( 'Announcement Link' ),
      'type' => 'text',
      'description' => __( 'Link set for announcement box on the homepage (if "Show Announcement Box" is checked).' ),
      'section' => 'options_settings',
    ) );

    $wp_customize->add_setting( 'announcement_copy', array(
      'type' => 'theme_mod',
      'default' => "",
    ) );
    $wp_customize->add_control( 'announcement_copy', array(
      'label' => __( 'Announcement Message' ),
      'type' => 'textarea',
      'description' => __( 'Text to be displayed underneath the heading on the announcement box on the homepage (if "Show Announcement Box" is checked). Empty by default.' ),
      'section' => 'options_settings',
    ) );

    $wp_customize->add_setting( 'google_analytics', array(
      'type' => 'theme_mod',
      'default' => "UA-XXXXXX-XX",
    ) );
    $wp_customize->add_control( 'google_analytics', array(
      'label' => __( 'Google Analytics' ),
      'type' => 'text',
      'description' => __( 'Replace UA-XXXXXX-XX with your site\'s ID.' ),
      'section' => 'options_settings',
    ) );

    // $wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'image_control', array(
    // 'label' => __( 'Featured Home Page Image', 'theme_textdomain' ),
    // 'section' => 'media',
    // 'mime_type' => 'image',
    // ) ) );

    return $wp_customize;
  }
  add_action( 'customize_register','nass_customizer' );

	// check if page slug exists
	// function the_slug_exists($post_name) {
	// 	global $wpdb;
	// 	if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "' AND post_status = 'publish'", 'ARRAY_A')) {
	// 	    return true;
	// 	} else {
	// 	    return false;
	// 	}
	// }

  //Page Slug Body Class
  function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
      $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
  }
  add_filter( 'body_class', 'add_slug_body_class' );

  class NASS_Menu_Walker extends Walker_Nav_Menu {

      var $number = 1;

      function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
          $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

          $class_names = $value = '';

          $classes = empty( $item->classes ) ? array() : (array) $item->classes;
          $classes[] = 'menu-item-' . $item->ID;

          $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
          $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

          $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
          $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

          $output .= $indent . '<li' . $id . $value . $class_names .'>';

          $atts = array();
          $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
          $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
          $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
          $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

          $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

          $attributes = '';
          foreach ( $atts as $attr => $value ) {
              if ( ! empty( $value ) ) {
                  $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                  $attributes .= ' ' . $attr . '="' . $value . '"';
              }
          }

          $item_output = $args->before;
          $item_output .= '<a'. $attributes .'>';
          $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
          $item_output .= '</a>';
          $item_output .= $args->after;

          $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
      }

  }

?>
