<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
<?php
  if (has_post_thumbnail()) {
    $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
  } else {
    $featured_image_url = null;
  }
?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php if ($featured_image_url): ?>
      <?php echo '<div class="featured-image" style="background-image:url(' . $featured_image_url . ');"></div>'; ?>
    <?php endif; ?>

    <div class="article-wrapper">
      <article class="post with-sidebar" id="post-<?php the_ID(); ?>">


				<div class="heading-wrapper">
					<h2>Careers</h2>
				</div>

  			<?php /*posted_on();*/ ?>

  			<div class="entry">

          <h3><?php the_title(); ?></h3>

  				<?php the_content(); ?>

          <p class="listings-footnote">NASS is an equal opportunity employer. NASS conducts drug screening and background checks on applicants who accept employment offers.</p>

  				<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

  			</div>

        <aside class="sidebar">
          <h1>Ready to Apply for This Position?</h1>
          <p>Fax completed <a href="http://nassusa.com/NASS%20Employment%20Application.pdf">application form</a> and resume to <?php echo get_theme_mod( 'company_fax' ); ?>, or send as an attachment to <a href="mailto:rlamb@nassusa.com">rlamb@nassusa.com</a>.</p>
        </aside>

        <aside class="sidebar">
          <h1 class="caps">More Job Openings</h1>
          <?php echo do_shortcode('[list-jobs max="15"]'); ?>
          <!-- <a href="/careers" class="show-all">See all openings &raquo;</a> -->
        </aside>

        <aside class="sidebar nobg">
          <h1 class="caps">Important Links</h1>
          <a href="http://nassusa.com/NASS%20Employment%20Application.pdf">Application form</a>
        </aside>

  		</article>
    </div>

		<?php /*comments_template();*/ ?>

		<?php endwhile; endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
