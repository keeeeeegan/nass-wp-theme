<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
<?php
  if (has_post_thumbnail()) {
    $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
  } else {
    $featured_image_url = null;
  }
?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php if ($featured_image_url): ?>
      <?php echo '<div class="featured-image" style="background-image:url(' . $featured_image_url . ');"></div>'; ?>
    <?php endif; ?>

    <div class="article-wrapper">
  		<article class="post" id="post-<?php the_ID(); ?>">

				<div class="heading-wrapper">
					<h2>Press Release</h2>
				</div>

  			<div class="entry">

					<span class="posted-date"><?php the_time('F j, Y') ?></span>

					<h3><?php the_title(); ?></h3>

  				<?php the_content(); ?>

  				<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

  			</div>

  		</article>
    </div>

		<?php /*comments_template();*/ ?>

		<?php endwhile; endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
