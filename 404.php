<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>


<div class="article-wrapper">
	<article class="post" id="post-<?php the_ID(); ?>">


		<div class="heading-wrapper">
			<h2>Page Not Found</h2>
		</div>

		<div class="entry">

			I'm sorry, the page you were looking for couldn't be found.

		</div>

	</article>
</div>





<?php get_sidebar(); ?>

<?php get_footer(); ?>
