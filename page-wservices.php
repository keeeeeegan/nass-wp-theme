<?php
/**
 * Template Name: Services Listing
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
<?php
  if (has_post_thumbnail()) {
    $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
  } else {
    $featured_image_url = null;
  }
?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php if ($featured_image_url): ?>
      <?php echo '<div class="featured-image" style="background-image:url(' . $featured_image_url . ');"></div>'; ?>
    <?php endif; ?>

    <div class="article-wrapper">
  		<article class="post" id="post-<?php the_ID(); ?>">


				<div class="heading-wrapper">
					<h2><?php the_title(); ?></h2>
				</div>

  			<?php /*posted_on();*/ ?>

  			<div class="entry">

  				<?php the_content(); ?>
        </div>
          <?php // TODO: make this list configurable ?>
        <div class="services">
          <div class="service">
            <h3>Transformer<span class="dropdown-button"></span></h3>
            <ul>
              <li>Hot Oil Processing</li>
            	<li>Assemblies (All Manufacturing)</li>
            	<li>Condition Assessments</li>
            	<li>Thermal Upgrades</li>
            	<li>Life Extentions</li>
            	<li>Painting</li>
            	<li>LTC Maintenance</li>
            	<li>LTC Retrofits</li>
            	<li>Ragasketing</li>
            	<li>Program</li>
            	<li>Training</li>
            	<li>Parts</li>
              <li>Commissioning</li>
              <li>Maintenance</li>
              <li>Condition-based testing</li>
            </ul>
          </div>
          <div class="service">
            <h3 class="second">Breakers<span class="dropdown-button"></span></h3>
            <ul>
              <li>Oil</li>
            	<li>Gas</li>
            	<li>Vacuum</li>
            	<li>Metal Clad</li>
            	<li>Testing</li>
            	<li>Repairs</li>
            	<li>Upgrades</li>
            	<li>Change Outs</li>
            	<li>Training</li>
            	<li>Painting</li>
            	<li>Filtration Installations</li>
            	<li>Parts</li>
              <li>Commissioning</li>
              <li>Maintenance</li>
              <li>Condition-based testing</li>
            </ul>
          </div>
          <div class="service">
            <h3 class="third">Engineering<span class="dropdown-button"></span></h3>
            <ul>
              <li>Specialty Testing</li>
              <li>Protection and Control Services</li>
              <li>Relay Testing</li>
              <li>Operational / Functional Testing</li>
              <li>Field Verification</li>
              <li>Print Review / Verification</li>
              <li>Commissioning</li>
              <li>Maintenance</li>
              <li>Condition-based testing</li>
            </ul>
          </div>
        </div>

        <?php /*
        <div class="entry">
          <div class="more">
            <a href="#">Read more &raquo;</a>
          </div>
        </div> */ ?>

  		</article>
    </div>

		<?php /*comments_template();*/ ?>

		<?php endwhile; endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
