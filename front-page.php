<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
<?php
  if (has_post_thumbnail()) {
    $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
  } else {
    $featured_image_url = null;
  }
?>

<?php

// set_theme_mod('announcement_copy', "Nomos Systems joins the NASS family of companies");
// set_theme_mod('announcement_link', "https://nassusa.com/press-release/nomos-joins-nass-family/");
// set_theme_mod('hiring_link', "/careers");
// set_theme_mod('show_hiring', true);
// set_theme_mod('show_announcement', true);

?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php if ($featured_image_url): ?>
      <?php echo '<div class="featured-image" style="background-image:url(' . $featured_image_url . ');">'; ?>
				<?php if (get_theme_mod('show_hiring')): ?>
          <a class="homepage-header-box now-hiring" href="<?php echo get_theme_mod('hiring_link'); ?>">
            <?php if (get_theme_mod('hiring_label')): ?>
              <h2><?php echo get_theme_mod('hiring_label'); ?></h2>
            <?php else: ?>
              <h2>We're Hiring!</h2>
            <?php endif; ?>
            <?php if (get_theme_mod('hiring_copy')): ?>
              <p><?php echo get_theme_mod('hiring_copy'); ?></p>
            <?php else: ?>
							<p class="link">View Current Listings</p>
            <?php endif; ?>
						<p class="read-more">Read more &raquo;</p>
          </a>
        <?php endif; ?>

				<?php if (get_theme_mod('show_announcement') && get_theme_mod('announcement_copy')): ?>
          <a class="homepage-header-box announcement" href="<?php echo get_theme_mod('announcement_link'); ?>">
            <?php if (get_theme_mod('announcement_label')): ?>
              <h2><?php echo get_theme_mod('announcement_label'); ?></h2>
            <?php else: ?>
              <h2 class="hidden">Announcement</h2>
            <?php endif; ?>
            <?php if (get_theme_mod('announcement_copy')): ?>
              <p><?php echo get_theme_mod('announcement_copy'); ?></p>
            <?php endif; ?>
						<p class="read-more">Read more &raquo;</p>
          </a>
        <?php endif; ?>

      <?php echo '</div>'; ?>
    <?php endif; ?>

    <div class="article-wrapper">
      <aside class="heading-carousel">
        <?php // TODO: update this to be a configurable setting ?>
        <h2>Safety. Quality. Service.</h2>
        <p><?php echo get_theme_mod( 'company_description' ); ?></p>
      </aside>
    </div>

		<?php endwhile; endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
