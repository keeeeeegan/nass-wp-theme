window.nass = function() {

  var init = function() {
    $('.services').on('click', 'h3', function() {
      $this = $(this);
      $this.toggleClass('open');
      $this.parent().children('ul').toggleClass('show');
    });

    $('.nav').on('click', '.menu-button', function() {
      $this = $(this);
      $this.toggleClass('open');
      $this.parent().find('.menu').toggleClass('show');
    });
  }

  return {
    init: init
  };
}();
