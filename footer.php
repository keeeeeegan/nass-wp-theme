<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?>
		<footer class="footer" class="source-org vcard copyright" role="contentinfo">

			<?php /* TODO: Make this NASS-specific content editable through WP admin */ ?>
			<div class="footer-items">
				<div class="blurb">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
					<p><?php echo get_theme_mod( 'company_short_description' ); ?></p>
				</div>
				<div class="contact-location">
					<h3>Contact Us</h3>
					<dl>
						<dt>Phone:</dt> <dd><?php echo get_theme_mod( 'company_phone' ); ?></dd>
						<dt>Fax:</dt> <dd><?php echo get_theme_mod( 'company_fax' ); ?></dd>
					</dl>
					<address>
						<b>North American Substation Services, LLC</b><br />
						190 North Westmonte Drive<br />
						Altamonte Springs, Florida 32714<br />
					</address>
				</div>
				<div class="contact-form-container">
					<h3>Send Us a Message</h3>
					<?php echo do_shortcode('[contact-form]'); ?>
				</div>
			</div>

			<div class="footer-stripe">
			<span class="copyright">&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></span>
				<nav class="nav" role="navigation">
					<?php wp_nav_menu( array('theme_location' => 'secondary') ); ?>
				</nav>
			</div>
		</footer>

	</div>

	<?php wp_footer(); ?>


<!-- jQuery is called via the WordPress-friendly way via functions.php -->

<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>

<?php if (get_theme_mod('google_analytics')): ?>

	<!-- Asynchronous google analytics; this is the official snippet.-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', '<?php echo get_theme_mod('google_analytics'); ?>', 'auto');
	  ga('send', 'pageview');

	</script>
<?php endif; ?>

</body>

</html>
